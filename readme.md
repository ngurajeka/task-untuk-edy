# How to start

- Clone repository
- Run this command inside the project
```
php -S 0.0.0.0:9000 -t .
```
- See the result on browser http://0.0.0.0:9000
- Untuk akses profil http://0.0.0.0:9000/profil.php
